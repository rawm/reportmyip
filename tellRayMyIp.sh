#!/bin/bash

iam=$(whoami)
NAMESERVER='nameserver	8.8.8.8'
pythonScript="/home/rawm/scripts/tellRay.py"



checkNameServer(){
	
	
	cat /etc/resolv.conf | grep "8.8.8.8"
	good=$?

	if [ ! "$good" -eq 0 ]
	then
		echo "Checking nameserver"
		echo
		echo $NAMESERVER >> /etc/resolv.conf
	fi

	printf "Good is $good\n"

}

getIP(){
	currentIP=$(curl ifconfig.me)

	echo -e "fr0m $(hostname)\n" > /home/$iam/emailme.txt
	echo -e "Your IP: $currentIP" >> /home/$iam/emailme.txt
}

sendIt(){
	python $pythonScript

}


checkNameServer
getIP 
sendIt
